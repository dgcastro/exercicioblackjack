package com.company;

import java.util.Date;

public class Funcionario extends Pessoa{
    private String racf;
    private float salario;
    private Date dataDeContratacao;

    public Funcionario(String racf, float salario, Date dataDeContratacao, String nome, int idade, int cpf){
        super(nome, idade, cpf);
        this.racf = racf;
        this.salario = salario;
        this.dataDeContratacao = dataDeContratacao;
    }

    public String getRacf() {
        return racf;
    }

    public void setRacf(String racf) {
        this.racf = racf;
    }

    public float getSalario() {
        return salario;
    }

    public void setSalario(float salario) {
        this.salario = salario;
    }

    public Date getDataDeContratacao() {
        return dataDeContratacao;
    }

    public void setDataDeContratacao(Date dataDeContratacao) {
        this.dataDeContratacao = dataDeContratacao;
    }
}
