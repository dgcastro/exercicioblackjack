package com.company;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        Funcionario novoFuncionario = new Funcionario("gouvcas", (float) 99999.99, new Date(), "Danilo", 27, 123456789);

        try{ Thread.sleep(60000);} catch (InterruptedException ex){}

        System.out.println("Funcionario:" + novoFuncionario.getNome() +"\nIdade: "+ novoFuncionario.getIdade()+ "\nCPF: "+ novoFuncionario.getCpf());
        System.out.println("RACF:" + novoFuncionario.getRacf() + "\nSalário: " + novoFuncionario.getSalario());
        System.out.println("Data de Contratação: " + novoFuncionario.getDataDeContratacao());
    }
}
